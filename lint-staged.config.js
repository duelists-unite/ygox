/**
 * @param {string[]} files
 */
const lintAndFormat = (files) => [
  'yarn affected:lint --uncommitted --fix --parallel',
  'yarn format',
  `git add ${files.join(' ')}`,
];

module.exports = {
  '*.{json,js,ts,sccs,html}': lintAndFormat,
};
