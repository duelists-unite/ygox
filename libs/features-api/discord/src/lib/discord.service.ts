import { Injectable } from '@nestjs/common';
import { Client, ClientUser } from 'discord.js';

@Injectable({})
export class DiscordService extends Client {
  constructor() {
    super({});
  }
}
