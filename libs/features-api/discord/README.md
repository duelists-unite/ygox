# features-api-discord

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test features-api-discord` to execute the unit tests via [Jest](https://jestjs.io).
