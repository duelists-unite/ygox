# Session for NestJS

In order to use sessions there are two easy steps

1. Import the `SessionModule.forRoot()` into your `AppModule`

```
import { SessionModule } from '@duelists-unite/features-api/session';

@Module({
  imports: [
    ...
    SessionModule.forRoot({ production: environment.production, secret: environment.secret }),
  ],
})
export class AppModule {}
```

2. Use the `@User()` parameter decorator in Controllers or Resolvers whenever you need to know who is logged in

```
import { User, ActiveUser } from '@duelists-unite/features-api/session';

@Controller()
export class AppController {
  @Get()
  public async example(@User() user: ActiveUser) {
    return user;
  }
}
```

## Running unit tests

Run `ng test features-api-session` to execute the unit tests via [Jest](https://jestjs.io).
