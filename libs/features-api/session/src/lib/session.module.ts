import {
  DynamicModule,
  Global,
  HttpModule,
  Inject,
  MiddlewareConsumer,
  Module,
  NestModule,
} from '@nestjs/common';
import connectRedis from 'connect-redis';
import expressSession from 'express-session';
import passport from 'passport';
import { createClient } from 'redis';

import { SessionSerializer } from './common/session.serializer';
import { SessionConfig } from './models/session-config.model';
import { SessionService } from './session.service';
import { SESSION_CONFIG } from './session.tokens';

@Global()
@Module({})
export class SessionModule implements NestModule {
  constructor(@Inject(SESSION_CONFIG) private readonly config: SessionConfig) {}

  public static forRoot(options: SessionConfig): DynamicModule {
    return {
      module: SessionModule,
      imports: [HttpModule],
      providers: [
        SessionService,
        SessionSerializer,
        { provide: SESSION_CONFIG, useValue: options },
      ],
      exports: [SessionService],
    };
  }

  @Override()
  public configure(consumer: MiddlewareConsumer): void {
    const Store = connectRedis(expressSession);
    const client = createClient();

    consumer
      .apply(
        expressSession({
          secret: this.config.secret,
          resave: false,
          saveUninitialized: false,
          cookie: {
            secure: this.config.production,
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000,
          },
          rolling: true,
          store: new Store({ client }),
        }),
        passport.initialize(),
        passport.session()
      )
      .forRoutes('');
  }
}
