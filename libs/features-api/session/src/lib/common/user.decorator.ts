import { createParamDecorator, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';

export const User = createParamDecorator((data, request: Request) => {
  if (!request.user) throw new UnauthorizedException('You do not appear to be signed in!');

  return request.user;
});
