import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';

import { ActiveUser } from '../models/active-user.model';
import { Tokens } from '../models/tokens.model';
import { SessionService } from '../session.service';

@Injectable()
export class SessionSerializer extends PassportSerializer {
  constructor(private readonly auth: SessionService) {
    super();
  }

  @Override()
  public serializeUser(
    { accessToken, refreshToken }: ActiveUser,
    done: (err?: Error, tokens?: Tokens) => void
  ): void {
    done(undefined, { accessToken, refreshToken });
  }

  @Override()
  public async deserializeUser(
    { accessToken, refreshToken }: Tokens,
    done: (err?: Error, user?: ActiveUser) => void
  ): Promise<void> {
    const { id } = await this.auth.login(accessToken);
    const user = await this.auth.getUser({ discordID: id });
    done(undefined, { ...user, accessToken, refreshToken });
  }
}
