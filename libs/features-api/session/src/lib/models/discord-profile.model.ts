export interface DiscordProfile {
  id: string;
  username: string;
  avatar: string;
  discriminator: string;
  locale: string;
  mfa_enabled: boolean;
  flags: number;
}
