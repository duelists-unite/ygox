import { User } from '@prisma/client';
import { Tokens } from './tokens.model';

export interface ActiveUser extends User, Tokens {}
