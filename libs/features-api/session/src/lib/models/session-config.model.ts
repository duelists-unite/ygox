export interface SessionConfig {
  production: boolean;
  secret: string;
}
