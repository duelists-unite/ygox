import { HttpService, Injectable } from '@nestjs/common';
import { User, UserWhereUniqueInput } from '@prisma/client';
import { requestNewAccessToken, use } from 'passport-oauth2-refresh';
import { pluck } from 'rxjs/operators';

import { PrismaService } from '@duelists-unite/features-api/prisma';

import { DiscordProfile } from './models/discord-profile.model';

@Injectable()
export class SessionService {
  constructor(private readonly http: HttpService, private readonly prisma: PrismaService) {}

  public async getUser(where: UserWhereUniqueInput): Promise<User> {
    const user = await this.prisma.user.findOne({ where });
    if (user === null) throw new Error('Could not find user...');

    return user;
  }

  public async setUser({ discordID }: Pick<User, 'discordID'>): Promise<User> {
    return this.prisma.user.upsert({
      where: {
        discordID,
      },
      update: {},
      create: {
        discordID,
      },
    });
  }

  public async validateUser(discordID: string): Promise<boolean> {
    const user = await this.prisma.user.findOne({ where: { discordID } });

    return Boolean(user);
  }

  public async login(accessToken: string): Promise<DiscordProfile> {
    return this.http
      .get<DiscordProfile>('https://discordapp.com/api/users/@me', {
        headers: { Authorization: `Bearer ${accessToken}` },
      })
      .pipe(pluck('data'))
      .toPromise();
  }

  public useRefreshStrategy(...params: Parameters<typeof use>): void {
    use(...params);
  }

  public async refresh(refreshToken: string): Promise<[string, string]> {
    return new Promise((resolve, reject) => {
      requestNewAccessToken('discord', refreshToken, (err, newAccessToken, newRefreshToken) => {
        if (err !== null) reject(err);
        resolve([newAccessToken, newRefreshToken]);
      });
    });
  }
}
