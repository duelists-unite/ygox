import { Inject, Injectable, OnApplicationShutdown } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

import { PrismaConfig } from './prisma-config.model';
import { PRISMA_CONFIG } from './prisma.tokens';

@Injectable()
export class PrismaService extends PrismaClient implements OnApplicationShutdown {
  constructor(@Inject(PRISMA_CONFIG) options: PrismaConfig) {
    super({
      ...options,
    });
  }

  @Override()
  public onApplicationShutdown(): void {
    void this.disconnect();
  }
}
