import { DynamicModule, Global, Module } from '@nestjs/common';

import { PrismaConfig } from './prisma-config.model';
import { PrismaService } from './prisma.service';
import { PRISMA_CONFIG } from './prisma.tokens';

@Global()
@Module({})
export class PrismaModule {
  public static forRoot(options: PrismaConfig): DynamicModule {
    return {
      module: PrismaModule,
      providers: [
        PrismaService,
        {
          provide: PRISMA_CONFIG,
          useValue: options,
        },
      ],
      exports: [PrismaService],
    };
  }
}
