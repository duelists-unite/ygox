import 'tslint-override/angular-register';

export * from './lib/prisma-config.model';
export * from './lib/prisma.module';
export * from './lib/prisma.service';
