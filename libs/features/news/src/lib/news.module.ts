import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';

import { ArticleComponent } from './article/article.component';
import { ArticlesComponent } from './articles/articles.component';
import { NewsComponent } from './news.component';

@NgModule({
  imports: [CommonModule, MatCardModule],
  declarations: [ArticleComponent, ArticlesComponent, NewsComponent],
  exports: [ArticlesComponent, NewsComponent],
})
export class NewsModule {
  constructor() {}
}
