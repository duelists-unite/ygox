import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'news-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent implements OnInit {
  constructor() {}

  @Input()
  public title = 'Default Title';

  @Input()
  public body = 'Some body text...';

  @Override()
  public ngOnInit(): void {}
}
