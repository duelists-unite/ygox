import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { FetchArticlesGQL } from '@duelists-unite/api-interfaces';

import { Article } from './article.model';
import { ArticlesStore } from './articles.store';

@Injectable({ providedIn: 'root' })
export class ArticlesService {
  constructor(
    private readonly fetchArticles: FetchArticlesGQL,
    private readonly store: ArticlesStore
  ) {}

  public refresh(): Observable<Article[]> {
    return this.fetchArticles.watch().valueChanges.pipe(
      map(({ data }) => data.articles),
      tap((articles) => this.store.set(articles))
    );
  }

  public add(article: Article): void {
    this.store.add(article);
  }

  public update(id: ID, article: Partial<Article>): void {
    this.store.update(id, article);
  }

  public remove(id: ID): void {
    this.store.remove(id);
  }
}
