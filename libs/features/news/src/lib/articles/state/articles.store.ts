import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

import { Article } from './article.model';

export interface ArticlesState extends EntityState<Article> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'articles' })
export class ArticlesStore extends EntityStore<ArticlesState> {
  constructor() {
    super();
  }
}
