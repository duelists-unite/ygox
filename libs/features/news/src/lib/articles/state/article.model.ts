import { guid } from '@datorama/akita';
import { Article } from '@duelists-unite/api-interfaces';
export { Article } from '@duelists-unite/api-interfaces';

/**
 * Creates a new Article supplying default values
 */
export const createArticle = (params?: Partial<Article>): Article => ({
  id: guid(),
  title: '',
  body: '',
  ...params,
});
