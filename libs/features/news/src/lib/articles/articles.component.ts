import { Component, OnDestroy, OnInit } from '@angular/core';
import { ID } from '@datorama/akita';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { Article } from './state/article.model';
import { ArticlesQuery } from './state/articles.query';
import { ArticlesService } from './state/articles.service';

@Component({
  selector: 'news-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit, OnDestroy {
  constructor(private readonly query: ArticlesQuery, private readonly service: ArticlesService) {}

  public articles$ = this.query.selectAll();
  public isLoading$ = this.query.selectLoading();

  @Override()
  public ngOnInit(): void {
    this.service
      .refresh()
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  @Override()
  public ngOnDestroy(): void {}

  public remove(id: ID): void {
    this.service.remove(id);
  }

  public trackByID(_index: number, article: Article): ID {
    return article.id;
  }
}
