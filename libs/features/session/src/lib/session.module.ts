import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';

import { SessionConfig } from './config.model';
import { SESSION_CONFIG } from './session.tokens';
import { SessionService } from './state/session.service';

/**
 * Provides session service, guards, and store.
 */
@NgModule({
  imports: [CommonModule, HttpClientModule],
})
export class SessionModule {
  public static forRoot(options: SessionConfig): ModuleWithProviders<SessionModule> {
    return {
      ngModule: SessionModule,
      providers: [
        { provide: SESSION_CONFIG, useValue: options },
        {
          provide: APP_INITIALIZER,
          useFactory: (session: SessionService) => async () => session.refresh().toPromise(),
          deps: [SessionService],
          multi: true,
        },
        SessionService,
      ],
    };
  }
}
