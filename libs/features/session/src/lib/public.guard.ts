import { Inject, Injectable, OnDestroy } from '@angular/core';

import { CanActivate } from '@angular/router';
import { selectPersistStateInit } from '@datorama/akita';
import { combineLatest, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { SessionConfig } from './config.model';
import { SESSION_CONFIG } from './session.tokens';
import { SessionQuery } from './state/session.query';

@Injectable({
  providedIn: 'root',
})
export class PublicGuard implements CanActivate, OnDestroy {
  constructor(
    @Inject(SESSION_CONFIG) private readonly config: SessionConfig,
    private readonly query: SessionQuery
  ) {}

  private readonly redirectSubscription = this.query.loggedIn$.subscribe((loggedIn) => {
    if (loggedIn) window.location.href = this.config.redirect;
  });

  @Override()
  public ngOnDestroy(): void {
    this.redirectSubscription.unsubscribe();
  }

  @Override()
  public canActivate(): Observable<boolean> {
    return combineLatest([this.query.loggedIn$, selectPersistStateInit()]).pipe(
      map(([loggedIn]) => !loggedIn),
      tap((activated) => {
        if (!activated) window.location.href = this.config.redirect;
      })
    );
  }
}
