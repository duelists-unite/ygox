import { TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SessionGuard } from './session.guard';
import { SESSION_CONFIG } from './session.tokens';
import { SessionQuery } from './state/session.query';

describe('SessionGuard', () => {
  let guard: SessionGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SESSION_CONFIG, useValue: { authServer: '', redirect: '' } },
        SessionQuery,
      ],
    });
    guard = TestBed.inject(SessionGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
