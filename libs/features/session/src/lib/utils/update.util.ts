import { snapshotManager } from '@datorama/akita';

export const updateStore = ({ newValue }: StorageEvent) => {
  if (newValue !== null) snapshotManager.setStoresSnapshot(newValue, { skipStorageUpdate: true });
};
