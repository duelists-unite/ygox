import { InjectionToken } from '@angular/core';

import { SessionConfig } from './config.model';

export const SESSION_CONFIG = new InjectionToken<SessionConfig>('session.config');
