/**
 * authServer - A URL to the authentication server to verify auth state against
 *
 * redirect - A URL to redirect the user to if a route is not activated
 */
export interface SessionConfig {
  authServer: string;
  redirect: string;
}
