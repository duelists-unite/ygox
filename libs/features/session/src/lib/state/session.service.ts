import { Inject, Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

import { SessionConfig } from '../config.model';
import { SESSION_CONFIG } from '../session.tokens';

import { SessionStore } from './session.store';

@Injectable({ providedIn: 'root' })
export class SessionService {
  constructor(
    @Inject(SESSION_CONFIG) private readonly config: SessionConfig,
    private readonly http: HttpClient,
    private readonly store: SessionStore
  ) {}

  public refresh(): Observable<boolean> {
    this.store.setLoading(true);

    return this.http.get<boolean>(`${this.config.authServer}/authenticated`).pipe(
      tap((loggedIn) => this.store.update({ loggedIn })),
      tap(() => this.store.setLoading(false))
    );
  }

  public async logout(): Promise<boolean> {
    return this.http
      .post<undefined>(`${this.config.authServer}/logout`, {})
      .pipe(
        switchMap(() => this.refresh()),
        map((loggedIn) => !loggedIn)
      )
      .toPromise();
  }
}
