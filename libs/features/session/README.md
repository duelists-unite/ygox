# Session for Angular

In order to use sessions there are two easy steps

1. Import the `SessionModule.forRoot()` into your `AppModule`

```
import { SessionModule } from '@duelists-unite/features/session';

@NgModule({
  imports: [
    ...
    SessionModule.forRoot({ authServer: environment.auth, redirect: environment.login }),
  ],
})
export class AppModule {}
```

2. Inject and use `SessionService` and/or `SessionQuery`

```
import { SessionQuery, SessionService } from '@duelists-unite/features/session';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(
    private readonly sessionService: SessionService,
    private readonly sessionQuery: SessionQuery
  ) {}

  public user$ = this.sessionQuery.user$;

  public logout(): void {
    this.sessionService.logout();
  }
}
```

```html
<mat-card *ngIf="user$ | async; let user">
  <mat-card-header>
    <mat-card-title>{{ user.name }}</mat-card-title>
  </mat-card-header>
  <mat-card-content>
    {{ user.status }}
  </mat-card-content>
</mat-card>
```

## Running unit tests

Run `nx test features-session` to execute the unit tests.
