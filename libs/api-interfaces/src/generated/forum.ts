import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Article = {
  id: Scalars['String'];
  title: Scalars['String'];
  body: Scalars['String'];
};

export type ArticleCreateInput = {
  id: Scalars['String'];
  title: Scalars['String'];
  body: Scalars['String'];
};

export type ArticleOrderByInput = {
  id?: Maybe<OrderByArg>;
  title?: Maybe<OrderByArg>;
  body?: Maybe<OrderByArg>;
};

export type ArticleWhereInput = {
  id?: Maybe<StringFilter>;
  title?: Maybe<StringFilter>;
  body?: Maybe<StringFilter>;
  AND?: Maybe<Array<ArticleWhereInput>>;
  OR?: Maybe<Array<ArticleWhereInput>>;
  NOT?: Maybe<Array<ArticleWhereInput>>;
};

export type ArticleWhereUniqueInput = {
  id?: Maybe<Scalars['String']>;
};

export type Mutation = {
  createOneArticle: Article;
};

export type MutationCreateOneArticleArgs = {
  data: ArticleCreateInput;
};

export enum OrderByArg {
  Asc = 'asc',
  Desc = 'desc',
}

export type Query = {
  articles: Array<Article>;
};

export type QueryArticlesArgs = {
  where?: Maybe<ArticleWhereInput>;
  orderBy?: Maybe<ArticleOrderByInput>;
  skip?: Maybe<Scalars['Int']>;
  after?: Maybe<ArticleWhereUniqueInput>;
  before?: Maybe<ArticleWhereUniqueInput>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type StringFilter = {
  equals?: Maybe<Scalars['String']>;
  not?: Maybe<Scalars['String']>;
  in?: Maybe<Array<Scalars['String']>>;
  notIn?: Maybe<Array<Scalars['String']>>;
  lt?: Maybe<Scalars['String']>;
  lte?: Maybe<Scalars['String']>;
  gt?: Maybe<Scalars['String']>;
  gte?: Maybe<Scalars['String']>;
  contains?: Maybe<Scalars['String']>;
  startsWith?: Maybe<Scalars['String']>;
  endsWith?: Maybe<Scalars['String']>;
};

export type FetchArticlesQueryVariables = {};

export type FetchArticlesQuery = { articles: Array<Pick<Article, 'id' | 'title' | 'body'>> };

export const FetchArticlesDocument = gql`
  query FetchArticles {
    articles {
      id
      title
      body
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class FetchArticlesGQL extends Apollo.Query<
  FetchArticlesQuery,
  FetchArticlesQueryVariables
> {
  document = FetchArticlesDocument;
}
