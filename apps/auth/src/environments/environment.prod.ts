export const environment = {
  production: true,
  api: 'https://auth-api.duelistsunite.org',
  redirect: 'https://duelistsunite.org',
};
