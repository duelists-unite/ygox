import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';

import { PublicGuard, SessionModule } from '@duelists-unite/features/session';

import { environment } from '../environments';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([{ path: '', component: LoginComponent, canActivate: [PublicGuard] }]),
    MatCardModule,
    MatButtonModule,
    FontAwesomeModule,
    SessionModule.forRoot({
      authServer: environment.api,
      redirect: environment.redirect,
    }),
  ],
  providers: [LoginService],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private readonly library: FaIconLibrary) {
    library.addIcons(faDiscord);
  }
}
