import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { switchMap, withLatestFrom } from 'rxjs/operators';

import { SessionQuery, SessionService } from '@duelists-unite/features/session';

import { environment } from '../../environments';
import { LoginService } from './login.service';

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly session: SessionService,
    private readonly sessionQuery: SessionQuery,
    private readonly service: LoginService
  ) {}

  @Override()
  public ngOnInit(): void {
    this.service.loginEvent$
      .pipe(
        switchMap(() => this.session.refresh()),
        untilDestroyed(this)
      )
      .subscribe();

    this.sessionQuery.loggedIn$
      .pipe(withLatestFrom(this.route.queryParamMap), untilDestroyed(this))
      .subscribe(([loggedIn, params]) => {
        if (loggedIn) window.location.href = params.get('redirect') ?? environment.redirect;
      });
  }

  @Override()
  public ngOnDestroy(): void {}

  public login(): void {
    this.service.openDiscordLogin(700, 500);
  }
}
