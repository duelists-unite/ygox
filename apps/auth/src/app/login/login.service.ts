import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { filter } from 'rxjs/operators';

import { environment } from '../../environments';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor() {}

  private loginPopup?: Window | null = undefined;
  public readonly loginEvent$ = fromEvent<MessageEvent>(window, 'message').pipe(
    filter((event) => event.source === this.loginPopup)
  );

  public openDiscordLogin(height: number, width: number): void {
    this.loginPopup?.close();
    this.loginPopup = window.open(
      environment.api,
      '',
      `
        toolbar=no,
        location=no,
        directories=no,
        status=no,
        menubar=no,
        scrollbars=no,
        copyhistory=no,
        height=${height},
        width=${width},
        top=${screen.height / 2 - width / 2},
        left=${screen.width / 2 - height / 2}`
    );
  }
}
