import dotenv from 'dotenv';
dotenv.config();

const host = 'http://localhost';
const port = 3333;
const prefix = 'api';

export const environment = {
  host,
  port,
  prefix,
  production: false,
  address: `${host}:${port}/${prefix}`,
  databaseURL: `${process.env.DATABASE_URL}`,
  secret: `${process.env.SECRET}`,
};
