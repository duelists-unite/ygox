const host = 'forum-api.duelistsunite.org';
const port = Number(process.env.PORT);
const prefix = '';

export const environment = {
  host,
  port,
  prefix,
  production: false,
  address: `${host}:${port}/${prefix}`,
  databaseURL: `${process.env.DATABASE_URL}`,
  secret: `${process.env.SECRET}`,
};
