import { environment } from './environment';
export { environment } from './environment';

switch (true) {
  case !environment.host:
  case !environment.port:
    throw new Error('You are missing the required host configuration from your environment...');
  case !environment.databaseURL:
    throw new Error('You are missing a Database URL from your environment...');
  case !environment.secret:
    throw new Error('You are missing the required secret from your environment...');
  default:
}
