import { Injectable } from '@nestjs/common';
import { GqlModuleOptions, GqlOptionsFactory } from '@nestjs/graphql';
import { MemcachedCache } from 'apollo-server-cache-memcached';

import { PrismaService } from '@duelists-unite/features-api/prisma';
import { environment } from '../../environments/environment';

@Injectable()
export class GraphQLConfig implements GqlOptionsFactory {
  constructor(private readonly prisma: PrismaService) {}

  @Override()
  public async createGqlOptions(): Promise<GqlModuleOptions> {
    return {
      autoSchemaFile: './libs/api-interfaces/src/graphql/forum/forum.graphql',
      path: '/api',
      context: ({ req }) => ({ req, prisma: this.prisma }),
      debug: !environment.production,
      persistedQueries: {
        cache: new MemcachedCache(
          ['memcached-server-1', 'memcached-server-2', 'memcached-server-3'],
          {
            retries: 10,
            retry: 10000,
          }
        ),
      },
      installSubscriptionHandlers: true,
      introspection: true,
      playground: !environment.production
        ? {
            settings: {
              'editor.cursorShape': 'line',
              'editor.fontFamily': `'Fira Code','Source Code Pro', 'Consolas', 'Inconsolata', 'Droid Sans Mono', 'Monaco', monospace`,
              'editor.fontSize': 14,
              'editor.reuseHeaders': true,
              'editor.theme': 'light',
              'general.betaUpdates': false,
              'queryPlan.hideQueryPlanResponse': false,
              'request.credentials': 'include',
              'tracing.hideTracingResponse': true,
            },
          }
        : false,
    };
  }
}
