import { Module } from '@nestjs/common';

import { CreateOneArticleResolver } from '@generated/typegraphql/resolvers/crud/Article/CreateOneArticleResolver';
import { FindManyArticleResolver } from '@generated/typegraphql/resolvers/crud/Article/FindManyArticleResolver';
import { RelationsResolversModule } from '@generated/typegraphql/resolvers/relations';

@Module({
  imports: [RelationsResolversModule],
  providers: [CreateOneArticleResolver, FindManyArticleResolver],
})
export class ArticlesModule {}
