import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { PrismaModule } from '@duelists-unite/features-api/prisma';
import { SessionModule } from '@duelists-unite/features-api/session';

import { environment } from '../environments';
import { GraphQLConfig } from './config/graphql.config';

import { ArticlesModule } from './articles/articles.module';

@Module({
  imports: [
    PrismaModule.forRoot({ debug: !environment.production }),
    GraphQLModule.forRootAsync({ useClass: GraphQLConfig }),
    SessionModule.forRoot({ production: environment.production, secret: environment.secret }),
    ArticlesModule,
  ],
  providers: [],
})
export class AppModule {}
