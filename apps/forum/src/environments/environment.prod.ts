export const environment = {
  production: true,
  api: 'https://forum-api.duelistsunite.org',
  auth: 'https://auth-api.duelistsunite.org',
  login: `https://auth.duelistsunite.org?redirect=${window.location.href}`,
};
