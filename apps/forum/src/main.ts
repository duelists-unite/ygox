import 'tslint-override/angular-register';

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableAkitaProdMode, persistState } from '@datorama/akita';
import { fromEvent } from 'rxjs';
import { filter } from 'rxjs/operators';

import { updateStore } from '@duelists-unite/features/session';

import { AppModule } from './app/app.module';
import { environment } from './environments';

if (environment.production) {
  enableProdMode();
  enableAkitaProdMode();
}

persistState({ key: 'session', include: ['session'] });
fromEvent<StorageEvent>(window, 'storage')
  .pipe(filter((event) => event.key === 'session'))
  .subscribe(updateStore);

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => {
    console.error(err);
  });
