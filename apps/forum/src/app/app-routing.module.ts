import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { SessionGuard } from '@duelists-unite/features/session';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
    canActivate: [SessionGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
    }),
    AkitaNgRouterStoreModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
