import { Component } from '@angular/core';

import { SessionService } from '@duelists-unite/features/session';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(private readonly session: SessionService) {}

  public logout(): void {
    this.session.logout();
  }
}
