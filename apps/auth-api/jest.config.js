module.exports = {
  name: 'auth-api',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/auth-api',
};
