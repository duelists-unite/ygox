import {
  Controller,
  Get,
  InternalServerErrorException,
  Post,
  Req,
  Session,
  UseGuards,
} from '@nestjs/common';
import { Express } from 'express';

import { ActiveUser, SessionService, User } from '@duelists-unite/features-api/session';

import { DiscordGuard } from './passport/discord.guard';

@Controller()
export class AuthController {
  constructor(private readonly session: SessionService) {}

  @Get()
  @UseGuards(DiscordGuard)
  public async discordLogin(): Promise<string> {
    return `<script>
              window.opener.postMessage('login', '*');
              window.close();
            </script>`;
  }

  @Get('authenticated')
  public async authenticated(@Req() req: Express.Request): Promise<boolean> {
    return Boolean(req.user);
  }

  @Post('refresh')
  public async refresh(
    @User() user: ActiveUser,
    @Session() session: Express.Session
  ): Promise<void> {
    const [accessToken, refreshToken] = await this.session.refresh(user.refreshToken);
    session.passport.user.accessToken = accessToken;
    session.passport.user.refreshToken = refreshToken;
    session.save((err) => {
      if (err) throw new InternalServerErrorException('Error when saving tokens...');
    });
  }

  @Post('logout')
  public logout(@Session() session: Express.Session): void {
    session.destroy(() => {});
  }
}
