import { Module } from '@nestjs/common';

import { AuthController } from './auth.controller';
import { DiscordStrategy } from './passport/discord.strategy';

@Module({
  providers: [DiscordStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
