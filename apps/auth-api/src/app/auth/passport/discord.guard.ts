import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

import { DISCORD_STRATEGY } from './discord.strategy';

@Injectable()
export class DiscordGuard extends AuthGuard(DISCORD_STRATEGY) implements CanActivate {
  @Override()
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const result = (await super.canActivate(context)) as boolean;
    const request = context.switchToHttp().getRequest<Request>();
    await super.logIn(request);

    return result;
  }
}
