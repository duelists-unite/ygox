import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from '@oauth-everything/passport-discord';

import { ActiveUser, SessionService } from '@duelists-unite/features-api/session';

import { environment } from '../../../environments';

export const DISCORD_STRATEGY = 'discord';

@Injectable()
export class DiscordStrategy extends PassportStrategy(Strategy, DISCORD_STRATEGY) {
  constructor(private readonly session: SessionService) {
    super({
      clientID: environment.discordID,
      clientSecret: environment.discordSecret,
      callbackURL: environment.address,
      scopes: ['identify'],
    });
    this.session.useRefreshStrategy(DISCORD_STRATEGY, this);
  }

  public async validate(
    accessToken: string,
    refreshToken: string,
    profile: Profile
  ): Promise<ActiveUser> {
    const user = await this.session.setUser({ discordID: profile.id });
    if (!user.discordID) throw new UnauthorizedException('Could not Authorize');

    return { ...user, accessToken, refreshToken };
  }
}
