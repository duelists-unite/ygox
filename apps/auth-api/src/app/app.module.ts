import { Module } from '@nestjs/common';

import { PrismaModule } from '@duelists-unite/features-api/prisma';
import { SessionModule } from '@duelists-unite/features-api/session';

import { environment } from '../environments';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    PrismaModule.forRoot({ debug: !environment.production }),
    SessionModule.forRoot({ production: environment.production, secret: environment.secret }),
    AuthModule,
  ],
})
export class AppModule {}
