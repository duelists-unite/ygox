const host = 'auth-api.duelistsunite.org';
const port = Number(process.env.PORT);
const prefix = '';

export const environment = {
  host,
  port,
  prefix,
  production: false,
  address: `${host}:${port}/${prefix}`,
  discordID: `${process.env.DISCORD_CLIENT_ID}`,
  discordSecret: `${process.env.DISCORD_CLIENT_SECRET}`,
  databaseURL: `${process.env.DATABASE_URL}`,
  secret: `${process.env.SECRET}`,
};
