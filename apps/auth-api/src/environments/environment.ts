import dotenv from 'dotenv';
dotenv.config();

const host = 'http://localhost';
const port = 3200;
const prefix = 'auth';

export const environment = {
  host,
  port,
  prefix,
  production: false,
  address: `${host}:${port}/${prefix}`,
  discordID: `${process.env.DISCORD_CLIENT_ID}`,
  discordSecret: `${process.env.DISCORD_CLIENT_SECRET}`,
  databaseURL: `${process.env.DATABASE_URL}`,
  secret: `${process.env.SECRET}`,
};
