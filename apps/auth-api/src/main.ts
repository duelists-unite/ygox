import 'tslint-override/angular-register';

import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';

import { environment } from './environments';

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(environment.prefix);
  await app.listen(environment.port, () => {
    console.log(`Listening at ${environment.address}`);
  });
};

bootstrap();
