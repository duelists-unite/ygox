import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const connect = () => prisma.connect();
export const disconnect = () => prisma.disconnect();

export const seed = async () => {
  await prisma.user.update({ data: { name: 'Test User' }, where: { id: 'test' } });
};

export const clean = async () => {
  await prisma.article.deleteMany({ where: {} });
  await prisma.user.deleteMany({ where: {} });
};
