# CCG X Projects

## Details

CCG X (Collecting Card Game X) is a project that aims to act as a fully featured modern platform for online game simulators. This project is setup as a NX Monorepo built using Angular 9, NestJS 6, Prisma 2, Apollo 2, Akita, and many other modern frameworks and libraries.

This project is divided up into Libraries and Applications.

An Application is designed to be a fully featured service which can be deployed at a subdomain -- `<app>.duelistsunite.org`. The planned applications are:

| App          | Description                                                                         |
| ------------ | ----------------------------------------------------------------------------------- |
| auth         | Login and Registration Pages                                                        |
| auth-api     | Discord OAuth API for `auth`                                                        |
| cards-api    | API for fetching card information                                                   |
| ccgx         | Core platform, Deck Editor, Simulator, etc.                                         |
| ccgx-api     | API for running games                                                               |
| forum        | Forums system offering threads and other features to build upon what discord offers |
| forum-api    | API for interfacing with forums, news and other related data                        |
| pictures     | Admin portal for interfacing with image data                                        |
| pictures-api | API powering `pictures`                                                             |

A Library is a reusable module that can be used in an application. Examples of Libraries would include the `news` module or the `session` module which may be used in different applications. Like the `news` may appear in both the `forum` and `ccgx` home page. And the `session` may be used by every client application after login is complete.

## Developer Setup

Setting up the developer environment for this

### VS Code

Included in `extensions.json` are a few recommended extensions to make your life easier when working on this project. VS Code should automatically popup when opening this project prompting you to install these extensions.

### Database

Setup a local Postgress Database. I recommend using docker

```sh
docker pull postgres
docker run  --name pg-docker -e POSTGRES_PASSWORD=<password> -d -p 5432:5432  postgres
```

After rebooting you can run the following command to start the database again

```sh
docker start pg-docker
```

### Environment

Create the `.env` file in the root of this repo with the following variables

```sh
DATABASE_URL="<postgress-url>"
```

### Install

Run the following commands...

```sh
yarn install
```

### Migrate

Run the following commands...

```sh
yarn migrate:up
```

### Run

This project is setup with VS Code Debugging, install the [Chrome Debugger](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) then press F5 to start the project.

You can also use one of the following commands to start the project manually

```sh
yarn start # Starts the default project
yarn start:forum # Starts the forum and the forum-api
yarn start <project> # Starts a project by name
```

### Committing

This project is setup with `Husky` which will run a few commands prior to each commit.

First and foremost husky will check your commit message to see if it matches the standards for this project. Basically you want to start your message title with one of the following prefixes `CI`, `DOCS`, `FEAT`, `FIX`, `REFACTOR`, `REVERT`, `STYLE`, `TEST`, `UPDATE` followed by a colon (`:`). Your title should also not end with a period (`.`). There are a few other rules about length of the message, etc. `Commit Lint` will notify you if any of these errors exist.

Example of a valid message

```txt
FEAT: Card Search

Sort By:
- Name
- Type
- Description
- Effect
... etc
```

Second `Husky` will run TS Lint and Prettier on your code code fixing any errors it can automatically. If TS Lint or Prettier fail to fix problems your commit will fail and you will need to fix these manually before committing. I recommend using these extensions to fix problems as you go rather then waiting until you commit. [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin) [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

If all that checks out then your commit will be made and then you can push it up to GitHub

### CI/CD

With the power of NX, CI/CD becomes extremely powerfully allowing test, linting, etc. to only run on what has changed on any given PR or commit. Utilizing NX Cloud a cache can also be utilized to drastically reduce test time for the runner and developers

### Deployment

While deployment hasn't been setup yet the plan is to use [ngx-deploy-docker](https://www.npmjs.com/package/ngx-deploy-docker) to build docker images for each application then publish them to a docker registry where a server can roll out updates using Kubernetes
